import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { emergencia } from '../emergencia';
import { emergenciaService }  from '../emergencia.service';

@Component({
  selector: 'app-emergencia-detail',
  templateUrl: './emergencia-detail.component.html',
  styleUrls: ['./emergencia-detail.component.css']
})
export class emergenciaDetailComponent implements OnInit {

 @Input() emergencia: emergencia;
 

  constructor(
    private route: ActivatedRoute,
    private emergenciaService: emergenciaService,
    private location: Location
 ) { }

  ngOnInit() {
   this.getemergencia();
  }

  getemergencia(): void {
  const id = +this.route.snapshot.paramMap.get('id');
  this.emergenciaService.getemergencia(id)
    .subscribe(emergencia => this.emergencia = emergencia);
}

  goBack(): void {
    this.location.back();
  }
 save(): void {
    this.emergenciaService.updateemergencia(this.emergencia)
      .subscribe(() => this.goBack());
  }

}
