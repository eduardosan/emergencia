import { Component, OnInit } from '@angular/core';
import { emergencia } from '../emergencia';
import { emergenciaService } from '../emergencia.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  emergencias: emergencia[] = [];

  constructor(private emergenciaService: emergenciaService) { }

  ngOnInit() {
  this.getemergencias();

  }

  getemergencias(): void {
    this.emergenciaService.getemergencias()
  .subscribe(emergencias => this.emergencias = emergencias.slice(1, 5));
  }

}
