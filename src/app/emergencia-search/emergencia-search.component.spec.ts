import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergenciaSearchComponent } from './emergencia-search.component';

describe('EmergenciaSearchComponent', () => {
  let component: EmergenciaSearchComponent;
  let fixture: ComponentFixture<EmergenciaSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergenciaSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergenciaSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
