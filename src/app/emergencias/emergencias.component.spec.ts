import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { emergenciasComponent } from './emergencias.component';

describe('emergenciasComponent', () => {
  let component: emergenciasComponent;
  let fixture: ComponentFixture<emergenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ emergenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(emergenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
